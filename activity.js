// Objective 1
db.users.find({ $or: [{firstName: { $regex: 'S'}}, {lastName: { $regex: 'D'}}]}, { firstName: 1, lastName: 1, _id: 0});

// Objective 2
db.users.find({$and: [{age: {$gte: 70}}, {department: "HR"}]});

// Objective 3
db.users.find({$and: [{age: {$lte: 30}}, {firstName: {$regex:'e'}}]});